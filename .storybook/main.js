const path = require('path')

module.exports = {
  stories: ['../src/**/*.stories.mdx', '../src/**/*.stories.@(js|jsx|ts|tsx)'],
  addons: ['@storybook/addon-links', '@storybook/addon-essentials'],
  webpackFinal: (config) => {
    config.module.rules.push({
      test: /\.styl$/,
      use: ['style-loader', 'css-loader', 'stylus-loader'],
      include: path.resolve(__dirname, '../'),
    })

    // Return the altered config
    return config
  },
}
