# anima-app

Esta aplicación permite generar personajes y demás relacionados con _Anima: Beyond Fantasy_.
Está pensada para su funcionamiento directamente en web, sin ningún tipo de base de datos. Los componentes están desarrollados siguiendo ideas de programación funcional, y las partes visuales de los componentes están desarrolladas con React.

La base principal está en Typescript.

## Comandos de compilación y desarrollo

### `yarn start`

Ejecuta la aplicación en modo desarrollo.

Abre la URL [http://localhost:3000](http://localhost:3000) para ver la página en tu navegador.

La página se recargará si se modifica algún fichero.
También se verán los errores de linter en la consola.

### `yarn test`

Lanza el test runner en modo watch interactivo.

### `yarn run build`

Construye la aplicación para producción en la carpeta `build`
Crea el bundle en modo producción minimizado y optimizado para el máximo rendimiento.

