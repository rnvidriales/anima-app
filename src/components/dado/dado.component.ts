/* istanbul ignore file reason: se llama a librerías ya testeadas */
/* eslint-disable fp/no-mutation, fp/no-mutating-assign */

import daggy from 'daggy'

import { Funciones } from './dado.funciones'
import { Algebra } from './dado.algebra'
import { Dado, DadoStatic, DadoConstructor } from './interfaces/dado.interface'

// Asigna las propiedades del Dado iniciales
// eslint-disable-next-line fp/no-let
let _DadoComponent = daggy.tagged<Dado, DadoStatic, DadoConstructor>('Dado', [
  'total',
  'numTiradas',
  'valores',
  'modificadores',
  'caras',
  'opciones',
])

// Asigna las funciones del algebra
_DadoComponent = Algebra(_DadoComponent)

// Obtiene las funciones del dado
_DadoComponent = Funciones(_DadoComponent)

export const DadoComponent: DadoConstructor & DadoStatic = _DadoComponent
