import Setoide from '../../../common/functional/algebra/interfaces/setoide.interface'
import { Modificador } from '../modificador/modificador.interface'
import { OpcionesDado } from './dado.opciones.interface'
import { BehaviorSubject } from 'rxjs'

export type DadoProps = [
  // total
  number,
  //numTiradas
  number,
  // valores
  number[],
  //modificadores
  Modificador[],
  // caras
  number,
  // opciones
  OpcionesDado,
]
export type DadoConstructor = (...args: DadoProps) => Dado

export interface DadoBaseCamposRequeridos {
  caras: number
  opciones: OpcionesDado
}
export interface DadoBase extends DadoBaseCamposRequeridos {
  total: number
  numTiradas: number
  valores: number[]
  modificadores: Modificador[]
  constructor(): DadoConstructor
}

export interface DadoFunctions {
  añadeModificador: (modificador: Modificador) => Dado
  lanzar: () => Dado
}

export type DadoÁlgebra = Setoide

export type Dado = DadoBase & DadoFunctions & DadoÁlgebra

export type DadoFnGen = {
  ({ caras, opciones }: DadoBaseCamposRequeridos): Dado
  (obj: {}): DadoFnGen
  ({ opciones }: { opciones: OpcionesDado }): DadoFnGen
  ({ caras }: { caras: number }): DadoFnGen
}

export interface DadoStatic {
  lanzar: DadoFnGen
  vacío: DadoFnGen
  copiar: DadoFnGen
}

export type Dado$ = BehaviorSubject<Dado | undefined>
