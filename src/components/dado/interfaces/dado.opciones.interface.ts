import { TiposDistribución } from '../../../common/math'

export interface OpcionesDado {
  /**
   * distribución de las tiradas aleatorias
   * - uniforme: todas las tiradas tienen la misma probabilidad
   * - normal: las tiradas siguen una distribución Gaussiana, los extremos son
   * muy difíciles de obtener
   */
  distribución: TiposDistribución
}
