/* eslint-disable fp/no-this, fp/no-mutation */

import * as R from 'ramda'

import { OpcionesDado } from './interfaces/dado.opciones.interface'
import { distribuciones } from '../../common/math'
import { Modificador } from './modificador/modificador.interface'
import { curryNamed } from '../../common/functional/curry-named'
import {
  Dado,
  DadoFnGen,
  DadoStatic,
  DadoConstructor,
} from './interfaces/dado.interface'

export const Funciones = (DadoComponent: DadoConstructor & DadoStatic) => {
  /**
   * Lanza un dado de las caras que se indiquen por parámetro (100 caras si no se
   * especifica)
   *
   * @param caras Número de caras que tendrá el dado que se tire
   * @returns número obtenido entre 1 y el número de caras
   */
  const generarTiradaAleatoria = function ({
    caras,
    opciones,
  }: {
    caras: number
    opciones: OpcionesDado
  }): number {
    const primerNumero = 1

    return distribuciones[opciones.distribución](primerNumero)(caras)
  }

  /**
   * Lanza el dado
   */
  DadoComponent.lanzar = curryNamed(
    ['caras', 'opciones'],
    ({ caras, opciones }: { caras: number; opciones: OpcionesDado }) =>
      R.compose(function (valor: number) {
        return DadoComponent(valor, 1, [valor], [], caras, opciones)
      }, generarTiradaAleatoria)({ caras, opciones }),
  ) as DadoFnGen

  /**
   * Tirada sin ningún dado lanzado
   * ```{
   *   total: 0,
   *   numTiradas: 0,
   *   valores: [],
   *   modificadores: []
   * }```
   */
  DadoComponent.vacío = curryNamed(
    ['caras', 'opciones'],
    ({ caras, opciones }: { caras: number; opciones: OpcionesDado }) =>
      DadoComponent(0, 0, [], [], caras, opciones),
  ) as DadoFnGen

  /**
   *
   */
  DadoComponent.copiar = curryNamed(['caras', 'opciones'], (dado: Dado) =>
    DadoComponent(
      (dado.valores ? dado.valores : []).reduce(
        (total, valor) => valor + total,
        0,
      ) +
        (dado.modificadores ? dado.modificadores : []).reduce(
          (total, modificador) => modificador.valor + total,
          0,
        ),
      dado.valores ? dado.valores.length : 0,
      dado.valores ? [...dado.valores] : [],
      dado.modificadores ? [...dado.modificadores] : [],
      dado.caras,
      dado.opciones,
    ),
  ) as DadoFnGen

  /**
   * Añade al total del dado el modificador y lo almacena en el campo
   * `modificador`
   */
  DadoComponent.prototype.añadeModificador = function (
    modificador: Modificador,
  ): Dado {
    return DadoComponent(
      this.total + modificador.valor,
      this.numTiradas,
      this.valores,
      [...this.modificadores, modificador],
      this.caras,
      this.opciones,
    )
  }

  /**
   * Lanza de nuevo el dado y añade el resultado
   */
  DadoComponent.prototype.lanzar = function (): Dado {
    const nuevaTirada: Dado = DadoComponent.lanzar({
      caras: (this as Dado).caras,
      opciones: (this as Dado).opciones,
    })

    return DadoComponent(
      this.total + nuevaTirada.total,
      this.numTiradas + 1,
      [...this.valores, ...nuevaTirada.valores],
      [...this.modificadores],
      this.caras,
      this.opciones,
    )
  }

  return DadoComponent
}
