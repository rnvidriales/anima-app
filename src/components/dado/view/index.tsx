import React from 'react'

import { ComponentDataIn } from '../../../common/view/view.interface'
import { switchcaseF } from '../../../common/functional/switch-case'

import { Dado$ } from '../interfaces/dado.interface'
import { DadoViewTexto } from './dado-texto'

export type DadoVisualizaciones = 'texto'

export default function genVista(modoVisualización: DadoVisualizaciones) {
  return (({ dataIn }: ComponentDataIn<Dado$>) =>
    switchcaseF({
      cases: {
        texto: () => DadoViewTexto(dataIn),
      },
      defaultCase: <div>Sin visualización</div>,
      key: modoVisualización,
    })) as React.FC<ComponentDataIn<Dado$>>
}
