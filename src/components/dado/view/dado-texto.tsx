import React from 'react'
import { BehaviorSubject } from 'rxjs'
import * as R from 'ramda'

import './dado-texto.styl'

import {
  Dado,
  Dado$,
  DadoBaseCamposRequeridos,
} from '../interfaces/dado.interface'
import { DadoComponent } from '../dado.component'
import { Modificador } from '../../dado/modificador/modificador.interface'

/**
 * Primary UI component for user interaction
 */
export const DadoViewTexto: React.FC<Dado$> = (dado$: Dado$) => {
  const generaDado = (d: Dado): Dado =>
    DadoComponent(
      d.total,
      d.numTiradas,
      d.valores,
      d.modificadores,
      d.caras,
      d.opciones,
    )
  const cambiaSiEsDiferente: <T>(
    fnCambio: (d: T) => any,
  ) => (antiguo: T) => (nuevo: T) => T = (fnCambio) => (antiguo) => (nuevo) =>
    R.when(R.pipe(R.equals(antiguo), R.not), fnCambio)(nuevo)

  const [dado, setDado] = React.useState(generaDado(dado$.value))
  const [errorMsg, setErrorMsg] = React.useState('')

  React.useEffect(() => {
    const suscripción = dado$.subscribe({
      next: (nuevoDado) => {
        cambiaSiEsDiferente(setDado)(dado)(generaDado(nuevoDado))
      },
      error: (e: Error) => {
        setErrorMsg(e.message)
      },
    })

    return function unsubscribe() {
      suscripción.unsubscribe()
    }
  })

  return R.ifElse(
    R.isNil,
    () => <div className="dado-texto"></div>,
    (dado) => (
      <div className="dado-texto">
        <table>
          <tbody>
            <tr>
              <th>Total:</th>
              <td>{dado.total}</td>
            </tr>
            <tr>
              <th>Tiradas:</th>
              <td>{dado.numTiradas}</td>
            </tr>
            {dado.valores.length > 0 && (
              <tr>
                <th>Resultados obtenidos:</th>
                <td>{dado.valores.join(', ')}</td>
              </tr>
            )}
            {dado.modificadores.length > 0 && (
              <tr>
                <th>Modificadores:</th>
                <td>
                  <ul>
                    {dado.modificadores.map((modificador: Modificador) => {
                      return (
                        <li>
                          {modificador.valor} = {modificador.origen}
                        </li>
                      )
                    })}
                  </ul>
                </td>
              </tr>
            )}
            <tr>
              <th>Caras:</th>
              <td>{dado.caras}</td>
            </tr>
            <tr>
              <td colSpan={2}>
                <button
                  onClick={() =>
                    dado$.next(
                      (DadoComponent.vacío as (
                        d: DadoBaseCamposRequeridos,
                      ) => Dado)(dado),
                    )
                  }
                >
                  Reiniciar
                </button>
              </td>
            </tr>
            <tr>
              <td colSpan={2}>
                <button onClick={() => dado$.next(dado.lanzar())}>
                  Lanzar otro Dado
                </button>
              </td>
            </tr>
          </tbody>
        </table>
        {errorMsg === '' ? '' : <span className="error">{errorMsg}</span>}
      </div>
    ),
  )(dado)
}
