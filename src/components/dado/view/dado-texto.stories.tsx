/* eslint-disable fp/no-proxy */
import React from 'react'
import { BehaviorSubject } from 'rxjs'
import { Story, Meta } from '@storybook/react/types-6-0'
import { action } from '@storybook/addon-actions'

import { distribuciones } from '../../../common/math'
import DadoVista from './index'
import { Dado$, DadoFnGen } from '../interfaces/dado.interface'
import { DadoComponent } from '../dado.component'

const DadoTexto = DadoVista('texto')

// Este export por defecto determina en qué posición va tu historia en la lista de historias
export default {
  title: 'Ánima/Dado/Texto',
  component: DadoTexto,
} as Meta

// Este observable controla la actualización interna de los valores del componente cuando se cambian los valores de los controles
const dado$: Dado$ = new BehaviorSubject(undefined) as Dado$

const Template: Story<{
  caras: number
  distribución: keyof typeof distribuciones
  dadoGen: DadoFnGen
}> = (args) => {
  dado$.next(undefined)

  // crea el dado de base con las opciones de los controles
  const dado = args.dadoGen({
    caras: args.caras,
    opciones: {
      distribución: args.distribución,
    },
  })

  dado$.next(dado)

  React.useEffect(() => {
    // Obtiene la información lanzada por el dado
    const suscripción = dado$.subscribe((dado) => {
      action(`dado$: ${JSON.stringify(dado)}`)()
    })

    return () => suscripción.unsubscribe()
  })

  return <DadoTexto {...{ dataIn: dado$ }} />
}
const DadoArgTypes = {
  caras: { control: 'number' },
  distribución: {
    control: {
      type: 'select',
      options: Object.keys(distribuciones),
    },
  },
  dadoGen: {
    table: {
      disable: true,
    },
  },
}

export const DadoTextoVacío = Template.bind({})
DadoTextoVacío.args = {
  caras: 20,
  distribución: 'uniforme',
  dadoGen: DadoComponent.vacío,
}
DadoTextoVacío.argTypes = DadoArgTypes

export const DadoTextoLanzado = Template.bind({})
DadoTextoLanzado.args = {
  caras: 20,
  distribución: 'uniforme',
  dadoGen: DadoComponent.lanzar,
}
DadoTextoLanzado.argTypes = DadoArgTypes
