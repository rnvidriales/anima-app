import * as faker from 'faker'

import { mockModificador } from '../modificador/__tests__/modificador.common'
import { mockDado } from './dado.common'

import { Dado } from '../interfaces/dado.interface'
import { OpcionesDado } from '../interfaces/dado.opciones.interface'
import { Modificador } from '../modificador/modificador.interface'
import { DadoComponent } from '../dado.component'
import { llamaNVeces } from '../../../common/__tests__/utils'

describe('dado.funciones', () => {
  const obtieneValorDado = (dado: Dado): number => dado.total

  const mockOpciones: OpcionesDado = {
    distribución: 'uniforme',
  }

  describe('lanzar', () => {
    const numeroTiradasDePrueba = 100
    const min = 1

    const compruebaTirada = ({
      valorDevuelto,
      max,
      min,
    }: {
      valorDevuelto: number
      max: number
      min: number
    }) => {
      expect(typeof valorDevuelto).toBe('number')
      // Comprueba si el número no es un integer (float no son válidos)
      expect(valorDevuelto).toBe(Math.floor(valorDevuelto))
      expect(valorDevuelto).toBeLessThanOrEqual(max)
      expect(valorDevuelto).toBeGreaterThanOrEqual(min)
    }

    it(`prueba ${numeroTiradasDePrueba} tiradas de un dado con caras aleatorias`, () => {
      llamaNVeces(numeroTiradasDePrueba, () => {
        const caras = faker.random.number({ min: 1 })
        const valorDevuelto = obtieneValorDado(
          DadoComponent.lanzar({
            caras,
            opciones: mockOpciones,
          }),
        )
        compruebaTirada({ valorDevuelto, max: caras, min })
      })
    })
  })

  describe('vacío', () => {
    it(`Genera una tirada vacía`, () => {
      const caras = faker.random.number({ min: 1 })
      const dadoVacío = DadoComponent.vacío({
        caras,
        opciones: mockOpciones,
      })

      expect(dadoVacío.total).toBe(0)
      expect(dadoVacío.numTiradas).toBe(0)
      expect(dadoVacío.valores).toHaveLength(0)
      expect(dadoVacío.modificadores).toHaveLength(0)
    })
  })

  describe('copiar', () => {
    const expectFn = (val: any) => expect(typeof val).toBe('function')

    it('si faltan datos básicos, devuelve una función (faltan las caras o las opciones)', () => {
      const dadoOrigen: Dado = {} as any

      const copia = DadoComponent.copiar(dadoOrigen)

      expectFn(copia)
    })

    it('si faltan otros datos, los pone en cero', () => {
      const dadoOrigen: Dado = mockDado()

      const copia = DadoComponent.copiar({
        caras: dadoOrigen.caras,
        opciones: dadoOrigen.opciones,
      })

      expect(copia.total).toBe(0)
      expect(copia.numTiradas).toBe(0)
      expect(copia.valores).toEqual([])
      expect(copia.modificadores).toEqual([])
      expect(copia.caras).toBe(dadoOrigen.caras)
      expect(copia.opciones).toEqual(dadoOrigen.opciones)
    })

    it('si copia correcta', () => {
      const dadoOrigen: Dado = mockDado()
      const modificadorTest = mockModificador()
      dadoOrigen.modificadores = [modificadorTest]
      dadoOrigen.total += modificadorTest.valor

      const copia = (DadoComponent.copiar(dadoOrigen) as unknown) as Dado

      expect(copia.total).toBe(dadoOrigen.total)
      expect(copia.numTiradas).toBe(dadoOrigen.numTiradas)
      expect(copia.valores).toEqual(dadoOrigen.valores)
      expect(copia.modificadores).toEqual(dadoOrigen.modificadores)
      expect(copia.caras).toBe(dadoOrigen.caras)
      expect(copia.opciones).toEqual(dadoOrigen.opciones)
    })
  })

  describe('añadeModificador', () => {
    it(`añade un modificador al dado`, () => {
      const dadoOrigen: Dado = mockDado()
      const modificador: Modificador = mockModificador()

      const dadoModificado: Dado = dadoOrigen.añadeModificador(modificador)

      expect(dadoModificado.total).toBe(dadoOrigen.total + modificador.valor)
      expect(dadoModificado.modificadores.slice(-1)[0]).toEqual(modificador)
    })
  })

  describe('lanzar', () => {
    it(`Añade un lanzamiento al dado`, () => {
      const dadoOrigen: Dado = mockDado()

      const nuevoDado = dadoOrigen.lanzar()
      const últimoValor = nuevoDado.valores.slice(-1)[0]

      expect(nuevoDado.total).toBe(dadoOrigen.total + últimoValor)
      expect(nuevoDado.numTiradas).toBe(dadoOrigen.numTiradas + 1)
      expect(nuevoDado.valores).toEqual([...dadoOrigen.valores, últimoValor])
      expect(nuevoDado.modificadores).toEqual(dadoOrigen.modificadores)
      expect(nuevoDado.caras).toBe(dadoOrigen.caras)
      expect(nuevoDado.opciones).toEqual(dadoOrigen.opciones)
    })
  })
})
