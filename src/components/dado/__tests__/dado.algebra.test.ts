import { testSetoide } from '../../../common/__tests__/algebra/setoide'
import { mockDado } from './dado.common'

import { Dado } from '../interfaces/dado.interface'

describe('dado.álgebra', () => {
  testSetoide(mockDado, (dado: Dado) => {
    const equivalente = mockDado()
    // eslint-disable-next-line fp/no-mutation
    equivalente.total = dado.total

    return equivalente
  })
})
