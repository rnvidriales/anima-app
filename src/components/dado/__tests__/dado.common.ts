import * as faker from 'faker'

import { Dado } from '../interfaces/dado.interface'
import { DadoComponent } from '../dado.component'

export function mockDado(): Dado {
  const tiradas = faker.random.number({
    min: 1,
  })
  const valores = Array(tiradas)
    .fill(0)
    .map(() =>
      faker.random.number({
        min: 1,
      }),
    )
  const total = valores.reduce((acc, x) => acc + x, 0)
  return DadoComponent(
    // total
    total,
    // numTiradas
    tiradas,
    // valores
    valores,
    // modificadores
    [],
    // caras
    faker.random.number({
      min: 1,
    }),
    // opciones
    {
      distribución: 'uniforme',
    },
  )
}
