export interface Modificador {
  // identificador del modificador (por si es necesario eliminarlo)
  id: string
  origen: string
  valor: number
}
