import * as faker from 'faker'

import { Modificador } from '../modificador.interface'

export function mockModificador(): Modificador {
  return {
    id: 'test',
    origen: 'Modificador de prueba: ' + faker.random.word(),
    valor: faker.random.number({
      min: 1,
    }),
  }
}
