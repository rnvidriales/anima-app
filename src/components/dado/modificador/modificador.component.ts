/* istanbul ignore file reason: se llama a librerías ya testeadas */

import daggy from 'daggy'

export const Modificador = daggy.tagged('Modificador', ['origen', 'valor'])
