/* eslint-disable fp/no-this, fp/no-mutation */

import * as FL from 'fantasy-land'
import { Dado, DadoStatic, DadoConstructor } from './interfaces/dado.interface'

export const Algebra = (DadoComponent: DadoConstructor & DadoStatic) => {
  /** Setoide */
  DadoComponent.prototype[FL.equals] = function (that: Dado): boolean {
    return this.total === that.total
  }
  return DadoComponent
}
