import * as FL from 'fantasy-land'

type Setoide = { [FL.equals]: <T>(fuente: T) => boolean }

export default Setoide
