import { curryNamed } from '../curry-named'

describe('curryNamed', () => {
  const getFn = ({ obj, k, add }: any): any => obj[k] + (add ? add : 0)
  const expectFn = (val: any) => expect(typeof val).toBe('function')

  it('Si no recibe argumentos, debe devolver undefined', () => {
    expect(curryNamed(undefined as any, undefined as any)).toBeUndefined()
  })

  it('happy path', () => {
    const getFnC = curryNamed(['obj', 'k', 'add'], getFn) as Function

    expectFn(getFnC)
    expectFn(getFnC({ obj: { a: 1 } }))
    expectFn(getFnC({ obj: { a: 1 }, k: 'a' }))
    expect(getFnC({ obj: { a: 1 }, k: 'a', add: 0 })).toBe(1)
    expect(getFnC({ obj: { a: 1 }, k: 'a' })({ add: 0 })).toBe(1)
  })
})
