import { switchcaseF } from '../switch-case'

describe('switchcase', () => {
  it('se cumple un case', () => {
    const mockCase = 'test'
    const mockDefaultFn = jest.fn()
    const value = switchcaseF({
      cases: { case: mockCase },
      defaultCase: mockDefaultFn,
      key: 'case',
    })
    expect(value).toBe(mockCase)
    expect(mockDefaultFn).not.toBeCalled()
  })
  it('se cumple un case (función)', () => {
    const mockCaseFn = jest.fn()
    const mockDefaultFn = jest.fn()
    switchcaseF({
      cases: { case: mockCaseFn },
      defaultCase: mockDefaultFn,
      key: 'case',
    })
    expect(mockCaseFn).toBeCalled()
    expect(mockDefaultFn).not.toBeCalled()
  })
  it('opción default', () => {
    const mockCaseFn = jest.fn()
    const mockDefaultFn = jest.fn()
    switchcaseF({
      cases: { case: mockCaseFn },
      defaultCase: mockDefaultFn,
      key: 'case2',
    })
    expect(mockCaseFn).not.toBeCalled()
    expect(mockDefaultFn).toBeCalled()
  })
})
