import * as R from 'ramda'

/**
 * Ejecuta la función pasada como parámetro o si no es una función, devuelve el
 * parámetro sin modificar
 *
 * @param f Función que se va a comprobar
 */
const executeIfFunction = <T>(f?: Function | T) =>
  f !== undefined && typeof f === 'function' ? (f as Function)() : f

/**
 * Ejecuta un switch/case funcional
 *
 * @param cases Opciones case del switch
 * @param defaultCase opción por defecto si ninguna se cumple
 * @param key variable a comprobar
 *
 * @returns valor del case que corresponda
 */
export const switchcase = ({
  cases,
  defaultCase,
  key,
}: {
  cases: Record<string | number, Function | any>
  defaultCase?: Function | any
  key: string | number
}) => (cases.hasOwnProperty(key) ? cases[key] : defaultCase)

/**
 * Función similar a `switchcase` pero en el caso de devolver una función, la
 * ejecuta
 */
export const switchcaseF = R.compose(executeIfFunction, switchcase)
