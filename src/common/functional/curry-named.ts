/**
 * Comprueba que la función tiene todos los argumentos para poder llamarse
 *
 * @param {array} listaArgumentos argumentos necesarios para llamar a la función
 * @param {array} argumentosActuales argumentos ya recibidos
 * @returns {boolean}
 */
function estáCompleta<T>(
  listaArgumentos: (keyof T)[],
  argumentosActuales: Record<keyof T, any>,
): boolean {
  return listaArgumentos.every(
    (argumento) => argumentosActuales[argumento] !== undefined,
  )
}

/**
 * Añade nuevos argumentos a los argumentos ya recibidos y los devuelve
 */
function añadeArgumentos<T>(
  nuevosArgumentos: Record<keyof T, any>,
  argumentosActuales: Record<keyof T, any>,
): Record<keyof T, any> {
  return Object.assign({}, nuevosArgumentos, argumentosActuales)
}

/**
 * @param {Function} fn Función a llamar
 * @param {array} listaArgumentos argumentos necesarios para llamar a la función
 * @param {array} argumentosActuales argumentos ya recibidos
 */
const llamaAFunción = <T>(
  fn: (args: T) => any,
  argumentosActuales: Record<keyof T, any>,
) => {
  return fn(argumentosActuales)
}

/**
 * Currifica una función con parámetros con nombre (la función debe recibir un
 * objeto con los parámetros necesarios para llamarse)
 *
 * @param camposNecesarios campos que necesita como argumento la función como
 * mínimo para ser llamada
 */
export function curryNamed<T>(
  camposNecesarios: (keyof T)[],
  fn: (args: T) => any,
) {
  if (
    camposNecesarios === null ||
    camposNecesarios === undefined ||
    Array.isArray(camposNecesarios) === false ||
    camposNecesarios.length === 0 ||
    typeof fn !== 'function'
  ) {
    return undefined
  }

  const currificar = (
    listaArgumentos: (keyof T)[],
    argumentosActuales: Record<keyof T, any>,
    fn: (args: T) => any,
  ) => (nuevosArgumentos: Record<keyof T, any>) => {
    const argumentos = añadeArgumentos(nuevosArgumentos, argumentosActuales)

    return estáCompleta(listaArgumentos, argumentos)
      ? llamaAFunción(fn, argumentos)
      : currificar(listaArgumentos, argumentos, fn)
  }

  return currificar(camposNecesarios, {} as any, fn)
}
