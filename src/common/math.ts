type FunciónRandom = (min: number) => (max: number) => number

/* istanbul ignore next reason: imposible testear de manera uniforme */
/**
 * Devuelve un número aleatorio en un rango abierto entre (0,1), es decir, no
 * incluye ni el 0 ni el 1
 */
const numeroAleatorioRangoAbierto = (): number => {
  const random = Math.random()
  return random === 0 ? numeroAleatorioRangoAbierto() : random
}

/* istanbul ignore next reason: imposible de testear de manera uniforme */
/**
 * Devuelve un número aleatorio en un rango abierto entre (0,1), siguiendo una
 * distribución Gaussiana
 */
const gauss = (): number => {
  const num =
    (Math.sqrt(-2.0 * Math.log(numeroAleatorioRangoAbierto())) *
      Math.cos(2.0 * Math.PI * numeroAleatorioRangoAbierto())) /
      10.0 +
    0.5
  return num > 1 || num < 0 ? gauss() : num
}

/* istanbul ignore next reason: imposible de testear de manera uniforme */
/**
 *
 * @param randomFn Función generadora de números aleatorios entre 0 y 1
 * @param min Límite inferior
 * @param max Límite superior
 */
const acotarRandom = (randomFn: () => number) => (min: number) => (
  max: number,
) => Math.floor(randomFn() * (max - min + 1) + min)

/* istanbul ignore next reason: wrapper */
/**
 * Funciones de distribución disponibles
 */
export const distribuciones = {
  uniforme: acotarRandom(Math.random) as FunciónRandom,
  normal: acotarRandom(gauss) as FunciónRandom,
}
export type TiposDistribución = keyof typeof distribuciones
