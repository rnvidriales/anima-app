import * as FL from 'fantasy-land'
import Setoide from '../../functional/algebra/interfaces/setoide.interface'

/**
 * Comprueba que el objeto pasado cumple las propiedades de un setoide
 */
export function testSetoide(
  setoide: () => Setoide,
  creaElementoSimilar: (fuente: any) => any,
) {
  describe('setoide', () => {
    it('reflexibilidad', () => {
      const a = setoide()
      expect(a[FL.equals](a)).toBeTruthy()
    })
    it('conmutabilidad', () => {
      const a = setoide()
      const b = creaElementoSimilar(a)

      expect(a[FL.equals](b)).toBe(b[FL.equals](a))
    })
    it('transitividad', () => {
      const a = setoide()
      const b = creaElementoSimilar(a)
      const c = creaElementoSimilar(b)

      expect(a[FL.equals](b) === b[FL.equals](c)).toBe(a[FL.equals](c))
    })
  })
}
