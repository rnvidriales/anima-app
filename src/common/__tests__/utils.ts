/**
 * Ejecuta una función num veces
 *
 * @param num número de iteraciones
 * @param fn función a ejecutar
 */
export const llamaNVeces = (n: number, fn: () => void): void =>
  Array(n).fill(0).forEach(fn)
