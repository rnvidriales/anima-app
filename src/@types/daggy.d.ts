/* eslint-disable fp/no-rest-parameters, @typescript-eslint/no-explicit-any */

declare module 'daggy' {
  export function tagged<Base, Static, Constructor>(
    name: string,
    fields: Array<keyof Base>,
  ): Constructor & Static
}
