# Para contribuir a este repositorio

¡Gracias por interesante en aportar a anima-app! Cualquiera que sea tu aportación:

- Reportar un bug
- Discutir el estado actual del código
- Subir un fix
- Proponer nuevas features
- Convertirse en un revisor

Antes de hacer cualquier cambio o desarrollo, por favor, primero es conveniente exponer la necesidad publicando un [issue](https://bitbucket.org/rnvidriales/anima-app/issues?status=new&status=open) explicando qué se quiere desarrollar o cuál es el problema encontrado.

Por favor, consulta el código de conducta antes de tener cualquier interacción con el proyecto.

## Estilo

Para espacios, convenciones de cuando usar o no coma, se utilizan herramientas como `prettier` o `ESLint`. El código se adaptará automáticamente y en algunos casos es necesario adaptarlo para que cumpla los estándares de Lint. Hay un fichero [`EditorConfig`](https://editorconfig.org/) para actuaalizar tu entorno de desarrollo a los estilos del proyecto.

## El desarrollo es en Bitbucket

Se usa Bitbucket para mantener el código, para hacer seguimiento de los issues, para hacer cambios y aceptar pull request. También tiene una wiki si es necesario publicar información acerca de la aplicación.

Para seguir el proceso de las tareas, hay un tablero Kanban en [Trello](https://trello.com/b/Jda2KgYs/anima-app)

## Pautas para los mensajes de commit (se usa el estándar de [Angular](https://github.com/angular/angular/blob/master/CONTRIBUTING.md))

Hay reglas muy precisas sobre cómo se formatean los mensajes de confirmación de git. Esto lleva a **más mensajes legibles** que son fáciles de seguir al mirar a través del **historial del proyecto**. Pero también, usamos los mensajes de confirmación de git para **generar el registro de cambios**.

### Formato de mensaje de commit

Cada mensaje de commit consta de un **encabezado**, un **cuerpo** y un **pie**. El encabezado tiene un formato especial que incluye un **tipo**, un **ámbito** y un **tema**:

```text
<tipo>(<ámbito>): <tema>
<LÍNEA EN BLANCO>
<cuerpo>
<LÍNEA EN BLANCO>
<pie>
```

El **encabezado** es obligatorio y el **ámbito** del encabezado es opcional.

Un mensaje de commit no puede tener más de 72 caracteres. Esto permite que el mensaje sea más fácil de leer en Bitbucket, así como en varias herramientas de git.

El pie debe contener una [referencia a un issue](https://help.github.com/articles/closing-issues-via-commit-messages/) si corresponde.

Ejemplos: (ver más [muestras](https://github.com/angular/angular/commits/master))

```text
docs (changelog): actualizar changelog a beta.5
```

```text
corregir (liberar): es necesario depender de los últimos rxjs y zone.js

La versión en nuestro package.json se copia a la que publicamos, y los usuarios necesitan la última versión.
```

### Revertir

Si el commit revierte un commit anterior, debe comenzar con `revert:`, seguido del encabezado de la confirmación revertida. En el cuerpo debe decir: `Esto revierte el commit <hash> .`, donde el hash es el SHA del commit que se está revertiendo.

### Tipo

Debe ser uno de los siguientes:

- **build**: Cambios que afectan el sistema de compilación o las dependencias externas (ejemplo de ámbitos: gulp, broccoli, npm)
- **ci**: Cambios en nuestros archivos de configuración de CI y scripts (ejemplo de ámbitos: Circle, BrowserStack, SauceLabs)
- **docs**: solo cambia la documentación
- **feat**: una nueva característica
- **fix**: Una corrección de errores
- **perf**: un cambio de código que mejora el rendimiento
- **refactor**: un cambio de código que no corrige un error ni agrega una característica
- **style**: Cambios que no afectan el significado del código (espacios en blanco, formato, semiconos que faltan, etc.)
- **test**: agregar pruebas faltantes o corregir pruebas existentes

### Ámbito

El alcance debe ser el nombre del componente afectado, o subcomponente

### Tema

El tema contiene una breve descripción del cambio:

- usar el imperativo: "cambia" no "cambiado" ni "cambios"
- no poner en mayúsculas la primera letra
- sin punto (.) al final

### Cuerpo

Al igual que en el **tema**, usar el imperativo. El cuerpo debe incluir la motivación para el cambio y contrastarlo con el comportamiento anterior.

### Pie

El pie debe contener cualquier información acerca de **cambios que rompen versiones anteriores** y es también el lugar para hacer referencia los problemas de Bitbucket que este commit **cierra**.

**Los cambios que rompen** deben comenzar con la palabra `BREAKING CHANGE:` con un espacio o dos saltos de línea.

## Se utiliza [Github Flow](https://guides.github.com/introduction/flow/index.html)

Para hacer cualquier contribución, se hace a través de pull request.

### Proceso de Pull Request

1. Haz un fork del repositorio, de la rama `develop` en tu cuenta.
2. Crea una rama usando bitbucket que resuelva un [issue](https://bitbucket.org/rnvidriales/anima-app/issues?status=new&status=open) ya generado (si no has creado un issue, es necesario crearlo para identificar el cambio que se está realizando). Para más información, revisa el apartado de creación de ramas.
3. Sigue las normas de estilo para poder crear un código común. Hay herramientas para controlar espaciados y los commit, pero naada para hacer nombres de variables y funciones. Para más información, ver el apartado de [estilo](#Estilo).
4. Asegurate de que cualquier dependencia de instalacción o build está eliminada antes siempre que hayas hecho un buuld del proyecto.
5. Comenta y justifica el cambio que se está realizando en laa descripción del pull request. Justifica con fotos si es necesario para entender mejor los cambios.
6. Para que se pueda aplicar tu cambio, debe ser aprobado por lo menos por un reviewer.

Cuando tu pull request se acepte, se generará una nueva versión de la app usando [SemVer](http://semver.org/).

## Cualquier contribución que haga estará bajo la licencia de software MIT

En resumen, cuando envía cambios de código, se entiende que sus envíos están bajo la misma [Licencia MIT](http://choosealicense.com/licenses/mit/) que cubre el proyecto.

## Código de Conducta convenido para Contribuyentes

### Nuestro compromiso

En el interés de fomentar una comunidad abierta y acogedora, nosotros como contribuyentes y administradores nos comprometemos a hacer de la participación en nuestro proyecto y nuestra comunidad una experiencia libre de acoso para todos, independientemente de la edad, dimensión corporal, discapacidad, etnia, identidad y expresión de género, nivel de experiencia, nacionalidad, apariencia física, raza, religión, identidad u orientación sexual.

### Nuestros estándares

Ejemplos de comportamiento que contribuyen a crear un ambiente positivo:

- Uso de lenguaje amable e inclusivo
- Respeto a diferentes puntos de vista y experiencias
- Aceptación de críticas constructivas
- Enfocarse en lo que es mejor para la comunidad
- Mostrar empatía a otros miembros de la comunidad

Ejemplos de comportamiento inaceptable por participantes:

- Uso de lenguaje o imágenes sexuales y atención sexual no deseada
- Comentarios insultantes o despectivos (_trolling_) y ataques personales o políticos
- Acoso público o privado
- Publicación de información privada de terceros sin su consentimiento, como direcciones físicas o electrónicas
- Otros tipos de conducta que pudieran considerarse inapropiadas en un entorno profesional.

### Nuestras responsabilidades

Los administradores del proyecto son responsables de clarificar los estándares de comportamiento aceptable y se espera que tomen medidas correctivas y apropiadas en respuesta a situaciones de conducta inaceptable.

Los administradores del proyecto tienen el derecho y la responsabilidad de eliminar, editar o rechazar comentarios, _commits_, código, ediciones de documentación, _issues_, y otras contribuciones que no estén alineadas con este Código de Conducta, o de prohibir temporal o permanentemente a cualquier colaborador cuyo comportamiento sea inapropiado, amenazante, ofensivo o perjudicial.

### Alcance

Este código de conducta aplica tanto a espacios del proyecto como a espacios públicos donde un individuo esté en representación del proyecto o comunidad. Ejemplos de esto incluye el uso de la cuenta oficial de correo electrónico, publicaciones a través de las redes sociales oficiales, o presentaciones con personas designadas en eventos _online_ u _offline_. La representación del proyecto puede ser clarificada explicitamente por los administradores del proyecto.

### Aplicación

Ejemplos de abuso, acoso u otro tipo de comportamiento inaceptable puede ser reportado al equipo del proyecto en [amima-app@googlegroups.com](mailto://amima-app@googlegroups.com). Todas las quejas serán revisadas e investigadas, generando un resultado apropiado a las circunstancias. El equipo del proyecto está obligado a mantener confidencialidad de la persona que reportó el incidente. Detalles específicos acerca de las políticas de aplicación pueden ser publicadas por separado.

Administradores que no sigan o que no hagan cumplir este Código de Conducta pueden ser eliminados de forma temporal o permanente del equipo administrador.

### Atribución

Este Código de Conducta es una adaptación del [Contributor Covenant][homepage], versión 1.4, disponible en https://www.contributor-covenant.org/es/version/1/4/code-of-conduct.html

[homepage]: https://www.contributor-covenant.org
